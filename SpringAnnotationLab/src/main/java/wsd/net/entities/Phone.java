package wsd.net.entities;

import org.springframework.stereotype.Component;

@Component
public class Phone {
    private String num;

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }

    public void init() {
        System.out.println("init " + this.toString());
    }

    public void destroy() {
        System.out.println(this.getClass().getName() + " destroy ");
    }

    @Override
    public String toString() {
        return "Phone{" +
                "num='" + num + '\'' +
                '}';
    }
}
