import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import wsd.net.entities.Students;

/**
 * Hello world!
 */
@ComponentScan("wsd.net")
public class App {
    public static void main(String[] args) {

        //Using annotation only
        Students s1 = new Students();
        System.out.println(s1);

        // Using Hybrid approach (xml+annotation)
        AbstractApplicationContext ac = new ClassPathXmlApplicationContext("Spring.xml");
        Students s = (Students) ac.getBean("studentBean");
        ac.close();

    }
}
