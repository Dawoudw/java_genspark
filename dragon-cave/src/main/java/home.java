import impl.*;

import java.util.Scanner;

public class home {
    public static void main(String[] args) {
        System.out.println(Messages.displayIntro());

        Scanner scanner = new Scanner(System.in);
        int input = 0;
        while (true) {
            System.out.println(Messages.displayOptions());
            try {
                input = scanner.nextInt();
            } catch (Exception e) {
                System.out.printf("\"Invalid input, Bye ...\"");

                scanner.close();
                break;
            }
            if (input == 1) {
                System.out.println(Messages.greedyDragonMsg());

            } else if (input == 2) {
                System.out.println(Messages.friendlyDragonMsg());

            } else {
                System.out.println(Messages.displayInvalidOption());
            }
            continue;

        }

    }
}
