package impl;

public abstract class Messages {
    static StringBuilder sb = null;

    public static String displayIntro()
    {
        sb = new StringBuilder("You are in a land full of dragons. In front of you,");
        sb.append(System.lineSeparator());
        sb.append("you see two caves. In one cave, the dragon is friendly");
        sb.append(System.lineSeparator());
        sb.append("and will share his treasure with you. The other dragon");
        sb.append(System.lineSeparator());
        sb.append("is greedy and hungry and will eat you on sight.");
        sb.append(System.lineSeparator());
        return sb.toString();
    }
    public static String greedyDragonMsg()
    {
        sb = new StringBuilder("You approach the cave...");
        sb.append(System.lineSeparator());
        sb.append("It is dark and spooky... ");
        sb.append(System.lineSeparator());
        sb.append("A large dragon jumps out in front of you! He opens his jaws and...");
        sb.append(System.lineSeparator());
        sb.append("Gobbles you down in one bite!");
        sb.append(System.lineSeparator());
        return sb.toString();
    }
    public static String friendlyDragonMsg()
    {
        sb = new StringBuilder("You approach the cave...");
        sb.append(System.lineSeparator());
        sb.append("A friendly dragon is approaching in front of you...");
        sb.append(System.lineSeparator());
        sb.append("He smiles and...");
        sb.append(System.lineSeparator());
        sb.append("would like to shares his treasures with you...");

        sb.append(System.lineSeparator());
        return sb.toString();
    }
    public static String displayOptions()
    {
        sb = new StringBuilder("Which cave will you go into? (1 or 2)");
        sb.append(System.lineSeparator());
        return sb.toString();
    }
    public static String displayInvalidOption()
    {

        sb = new StringBuilder("Invalid choice, Which cave will you go into? (1 or 2)");
        sb.append(System.lineSeparator());
        return sb.toString();
    }
}
