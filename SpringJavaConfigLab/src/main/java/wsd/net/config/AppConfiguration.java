package wsd.net.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.AbstractApplicationContext;
import wsd.net.entities.Students;

@Configuration
@ComponentScan("wsd.net")
public class AppConfiguration {

    public Students getStudents() {
        return new Students();
    }

}
