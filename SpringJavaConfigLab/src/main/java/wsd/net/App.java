package wsd.net;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.support.AbstractApplicationContext;
import wsd.net.config.AppConfiguration;
import wsd.net.entities.Students;

@ComponentScan("wsd.net")
public class App {
    public static void main(String[] args) {

        AbstractApplicationContext ac = new AnnotationConfigApplicationContext(AppConfiguration.class);
        Students s = (Students) ac.getBean(Students.class);
        System.out.println(s);
        System.out.println("total beans:"+  ac.getBeanDefinitionCount());

        for (String str : ac.getBeanDefinitionNames() )
        {
            System.out.println(str);

        }

    }


}
